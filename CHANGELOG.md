# Changelog

Summary of release changes to mycicd-gitlab-docker

## 0.0.1
- contains the latest changes to the project. As new changes are added more information will be added to each version.

## 0.0.2

## 0.0.3
- added a script which displays the changes made by outputting the commits and tag information. 