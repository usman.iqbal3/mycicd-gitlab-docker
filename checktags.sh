# Update local tags with remote
git fetch --tags -f

#Latest commit:
git show --tags

#shows previous commits / merge requests
git log

#Lists tags with dates:
git log --tags --simplify-by-decoration --pretty="format:%ci %d"


echo "## Where to make changes, if there are any tags that have been updated, make sure they are referenced correctly :"
echo "If lambda was changed, make a change here: https://gitlab.com/dwp/data-as-a-service/platform/shared-services/infrastructure/modules/aws/shbe/-/blob/main/lambda.tf"
echo "If glue_jobs changed, make change here: https://gitlab.com/dwp/data-as-a-service/platform/shared-services/infrastructure/modules/aws/shbe/-/blob/main/glue_jobs.tf"
echo "If stepfunctions changed, make change here: https://gitlab.com/dwp/data-as-a-service/platform/shared-services/infrastructure/modules/aws/shbe/-/blob/main/step_functions.tf"
echo "If kms changed, make change here: https://gitlab.com/dwp/data-as-a-service/platform/shared-services/infrastructure/modules/aws/shbe/-/blob/main/kms.tf"
echo "If s3 was changed, make change here: https://gitlab.com/dwp/data-as-a-service/platform/shared-services/infrastructure/modules/aws/shbe/-/blob/main/s3.tf"

