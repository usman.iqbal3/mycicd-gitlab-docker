output "s3_bucket_id" {
  description = "Bucket name."
  value       = element(aws_s3_bucket.bckt.*.id, 0)
}