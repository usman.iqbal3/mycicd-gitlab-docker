variable "bucket" {
    type = string
    default = "mbt00107"
}

variable "s3_force_destroy" {
    default = true
}