package test

import (
	"testing"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
)

func TestS3(t *testing.T) {
	awsRegion := "us-west-2"
	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: "..",
		EnvVars: map[string]string{
			"AWS_DEFAULT_REGION": awsRegion,
		},
	})
// clean the resources with terraform destroy at end of test. 
defer terraform.Destroy(t, terraformOptions)

// runs terraform init and apply. Fails the test if there are any errors. 
terraform.InitAndApply(t, terraformOptions)

// Run `terraform output` to get the values of output variables and check they have the expected values.
bucket_id := terraform.Output(t, terraformOptions, "bucket_id")

// check if a bucket with the name mbt00107 has been created. 
assert.Equal(t, "mbt00107", bucket_id)
}
