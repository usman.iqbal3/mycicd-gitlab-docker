changelog=CHANGELOG.md

new_changelog()
{
  echo ""# Changelog
All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
## [Unreleased]"" > CHANGELOG.md
}
new_changelog_item()
{
  version = "$(git describe --long)"
  today="$(date '+%Y-%m-%d')"

  echo 'adding changelog item'
}
init()
{
  if test -f "$changelog" ; then
    echo "$changelog exists."
  else
    echo "$changelog does not exist."
  fi
}