# Create a Gitlab Instance within a Docker Container with a CI/CD Pipeline.
This will guide you to create a local Gitlab instance within Docker with a .gitlab-ci.yml configuration file to run a ci/cd pipeline.

## Create a docker container which contains a local gitlab instance
To initially setup the container run `./docker-compose.yml`.  
After initial setup the container can be started within terminal using the command `docker start gitlab`.  

Once the container is up - search for **http://localhost/users/sign_in** in your search engine web bar.   
You will be prompted to login.  

The username is **root**. To access the password run the command `docker exec -it gitlab grep 'Password:' /etc/gitlab/initial_root_password` in your terminal.
**Note this file will be deleted after 24 hours.** Be sure to reset password for root using the command below. 

This should display the root password. Use these login details to sign into gitlab on your local computer.  

## Resetting root password
To reset the root password enter `docker exec -it gitlab gitlab-rake "gitlab:password:reset[root]"` into command line. Usually there is a delay then it will prompt you to enter a new password.  
Once you are logged into gitlab you will notice an automatic repository will be created called **monitoring**, this is not relevant to this guide.
# Manually add a user
As standard you will be logged in as **root** which has admin access.

To register a new user, in the top left near the gitlab logo, there is a option called **menu**, click on that. A tab will open, go down to the last option called **admin** - there is a spanner logo next to it. 

Once opened you are into the gitlab instance admin area,
-   click on add user 

Here you can add as many users as you like. 

# How to create a repository
To create a repository click on the *square box with the plus sign inside*, this is located on the left hand side of the *Search Gitlab* tab on the top right hand side of your screen. 
- click **New project/repository**
- click **Create blank project**
- enter details and **create project**

# How to pull, push and commit code. 
Before you can push, pull or commit, the server has to be live and running. **Check on docker to see if it is running** - can run `docker ps` in terminal to view running containers. 
## Set SSH Keys.
It's important to set your ssh keys in your profile as this will allow you to pull and push code from and to the repository. To do this follow this guide in gitlab docs : ***https://docs.gitlab.com/ee/user/ssh.html***

The commands to pull, push, add and commit to your repository:
- `git pull` is a git command to update the local version of a repository from  a remote repo. 
- `git push` - git push is used to upload local repository content to a remote repo. 
- `git commit` -  is a snapshot of your directory at the time.
- `git add` - this command adds file/s to a staging area ready to be committed. 

# Clone this repository into your Visual Studio code/IDE 
To clone a repository:
 - Be in the overview of you chosen repository.
 - Click on **Clone**
 - Copy **Clone with SSH** - **'git@localhost:root/cicd-gitlab-docker.git'** *for this repo*
 - In you terminal `cd gitlab`, and run this command `git clone git@localhost:root/cicd-gitlab-docker.git` 
 
 # Install Gitlab-Runner
Install Gitlab-Runner on your container CLI. This can be accessed on Docker Desktop - hover over container, on the right there are options click on CLI.

```
{
### Download Binary:
curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-386

### Give permissions to execute
chmod +x /usr/local/bin/gitlab-runner

### Create a Gitlab CI user
useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

### Install Gitlab-Runner
gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

### Register a Runner
gitlab-runner register

^follow the steps - when it comes to choosing the executor tap in 'shell'

### Start the runner
gitlab-runner start
}
```
https://docs.gitlab.com/runner/install/osx.html

# Pointing the gitlab-runner to the direction of executable files. 
use command `whoami` to ensure you are root within your container CLI. 
```
{
    #Change user to gitlab-runner
    su - gitlab-runner 

    #Prints out current path. 
    echo $PATH

    #Lists directory contents
    ls -la

    #Allows you to insert into the .profile file. 
    vi ./profile
    press i to insert changes
    insert `PATH=$PATH:/opt/gitlab/embedded/bin` at the bottom of the file. 
}
```
# Configure AWS Credentials
I have used the AWS Sandbox within Pluralsight. 

In your terminal type:
- `aws configure` -- it will ask for **AWS Access Key ID** and **AWS Secret Access Key** - copy and paste credentials from pluralsight.
- it will ask for default region name (keep it blank unless you have a specified region)
- default output format - press **Enter** to keep it blank. 


# .gitlab-ci.yml configuration
It is a simple CI/CD configuration file which sits at the root of the repository. It runs through 3 stages and prints out messages. 
### Configuration:

```
{
 #This is a simple CICD config file. Demos a 3 stages of the CI/CD pipeline and uses echos to print simple messages to the screen. 

stages:          # Executes in order.
  - pre-checks
  - build
  - test
  - deploy


#pre-checks:
  #stage: pre-checks
  #script:
    #- pip install pre-commit
      #`go mod init "<MODULE_NAME>"`where `MODULE_NAME` is the module's name,
   # - go mod init localhost/root/cicd-gitlab-docker
   # - go mod tidy
   # - pre-commit install
   # - pre-commit run --all-files
   
before_script:
##
  ## Set up Environment Variables
  ##
  #- export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root/go/bin:/usr/local/go/bin:/root/go/bin
  #- export GOPATH=/root/go

build-job:       # This job runs in the build stage, which runs first.
  stage: build
  script:
    - echo "Compiling the config..."
    - echo "Compile complete."
    - echo "$CI_JOB_STAGE"

test:
  stage: test
  script:
     ## Go to each deployment folder in repo and test
    ## `go mod init "<MODULE_NAME>"`where `MODULE_NAME` is the module's name,
    ## something like `gitlab.com/methods-platform-team/infrastructure/modules/aws/networking`
    - go mod init localhost/root/cicd-gitlab-docker
    - go mod tidy
    - cd test
    - go test -v -timeout 30m s3_test.go

#unit-test-job: 
  #stage: test    # It only starts when the job in the build stage completes successfully.
  #script:
    #- echo "Running unit tests... Could take around 30 seconds."
    #- sleep 60
    #- echo "test passed"

lint-test-job:   
  stage: test    # It can run at the same time as unit-test-job (in parallel).
  script:
    - echo "Linting code... will take about 15 seconds."
    - echo "issues found."

deploy-job:      # This job runs in the deploy stage.
  stage: deploy  # It only runs when *both* jobs in the test stage complete successfully.
  script:
    - echo "Deploying code..."
    - echo "Successfully deployed."

}
```






