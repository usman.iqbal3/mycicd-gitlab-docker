module "s3_bucket" {
    source = "./modules/s3"
    bucket1 = local.bucket
    force_destroy = var.s3_force_destroy
}

output "bucket_id" {
  value       = module.s3_bucket.s3_bucket_id
}
